import {defineAsyncComponent} from 'vue'
// @ts-ignore
import {SetupCalendar} from 'v-calendar'
import {plugin as VueTippy} from 'vue-tippy'
import {START_LOCATION} from 'vue-router'

import {useNotyf} from './composable/useNotyf'
import {useUserSession} from './stores/userSession'
import moment from "moment";
moment.locale('ru')
import type {AppContext} from './app'
import { createSocket } from './composable/useSocket'
declare module '@vue/runtime-core' {
    interface ComponentCustomProperties {
        // @ts-ignore
        $moment: moment
    }
}
const userSession = useUserSession()
const socket = createSocket()

// Lazy load aditional components
export async function registerGlobalComponents({app}: AppContext) {
    const background = (await import('./directives/background')).default
    const tooltip = (await import('./directives/tooltip')).default

    app.use(SetupCalendar, {})
    app.use(VueTippy, {
        component: 'Tippy',
        defaultProps: {
            theme: 'light',
        },
    })

    app.component(
        // eslint-disable-next-line vue/multi-word-component-names
        'Multiselect',
        defineAsyncComponent({
            loader: () => import('@vueform/multiselect').then((mod) => mod.default),
            delay: 0,
            suspensible: false,
        })
    )
    app.component(
        // eslint-disable-next-line vue/multi-word-component-names
        'Slider',
        defineAsyncComponent({
            loader: () => import('@vueform/slider').then((mod) => mod.default),
            delay: 0,
            suspensible: false,
        })
    )
    app.component(
        'VCalendar',
        defineAsyncComponent({
            loader: () => import('v-calendar').then((mod) => mod.Calendar),
            delay: 0,
            suspensible: false,
        })
    )
    app.component(
        'VDatePicker',
        defineAsyncComponent({
            loader: () => import('v-calendar').then((mod) => mod.DatePicker),
            delay: 0,
            suspensible: false,
        })
    )

    app.directive('background', background)
    app.directive('tooltip', tooltip)
    app.config.globalProperties.$moment = moment
    // @ts-ignore
    app.config.globalProperties.$declension = (number, titles) => {
        const cases = [2, 0, 1, 1, 1, 2];
        return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
    }

    // app.config.globalProperties.$socket = socket
    app.provide('socket', socket)
}

 
export function registerRouterNavigationGuards({router, api}: AppContext) {
    // @ts-ignore
    router.beforeEach(async (to, from) => {
        const notyf = useNotyf()
        if (from === START_LOCATION && userSession.isLoggedIn) {
            // @ts-ignore
            socket.emmit('auth:check')
            socket.on('auth:success', async (data) => {
                await userSession.setToken(data.token)
                await userSession.setToken(data.user)
                notyf.success(`С возвращением, ${userSession.user.full_name}`)
            })
            // @ts-ignore
        } else if (to.meta.requiresAuth && !userSession.isLoggedIn) {
            notyf.error({
                message: 'Sorry, you should loggin to access this section (anything will work)',
                duration: 7000,
            })
            return {
                name: 'auth-login',
                query: {redirect: to.fullPath},
            }
        }
    })
}
