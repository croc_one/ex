import {App, createApp as createClientApp, h, Suspense} from 'vue'

import {createHead} from '@vueuse/head'
import {createPinia} from 'pinia'
import {createI18n} from './i18n'
import {createRouter} from './router'
// @ts-ignore
import App from './App.vue'

import './styles'

import {initDarkmode} from '/@src/stores/darkmode'
import {apiUri, createApi} from '/@src/composable/useApi'

// @ts-ignore
export type AppContext = Awaited<ReturnType<typeof createApp>>

import {registerGlobalComponents, registerRouterNavigationGuards} from './app-custom'
import {useUserSession} from "/@src/stores/userSession";
export async function createApp() {
    const head = createHead()
    const i18n = createI18n()
    const router = createRouter()
    const pinia = createPinia()
    const api = createApi()
    const app = createClientApp({
        // This is the global app setup function
        setup() {
            /**
             * Initialize the darkmode watcher
             *
             * @see /@src/stores/darkmode
             */
            initDarkmode()
            const userSession = useUserSession()
            if (userSession.isLoggedIn) {
                api.get(apiUri().profile.get).then((response) => {
                    // @ts-ignore
                    userSession.user = response.data.user;
                    userSession.setLoading(false)
                }).catch(() => {
                    userSession.logoutUser()
                    // redirect the user somewhere
                    router.replace('/auth/login')
                })
            }
       
            return () => {
                return h(Suspense, null, {
                    default: () => h(App),
                })
            }
        },
    })
 
    const components = {
        app,
        api,
        router,
        i18n,
        head,
        pinia,
    }

    await registerGlobalComponents(components)
    app.use(components.pinia)
    app.use(components.head)
    app.use(components.i18n)
    registerRouterNavigationGuards(components)
    app.use(components.router)

    return components
}
