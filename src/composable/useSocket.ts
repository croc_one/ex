import {io} from 'socket.io-client'
import {useUserSession} from './stores/userSession'

// const currentDomain = window.location.host
// const currentDomainArray = currentDomain.split('.')
// let managePanel = '/v1';
// if (currentDomainArray.length >= 3) {
//     managePanel = currentDomainArray[0]
// }
let socket

// @ts-ignore
export function createSocket() {
    // @ts-ignore
    socket = io('http://localhost:3000', {query: `auth_token=${useUserSession.token}`})
     // @ts-ignore
    socket.onAny((event, ...args) => {
        console.log(event, args);
      });
    return socket
}
